# Fitur
    mudah diedit (fully editable, support libreoffice draw '.odg')
    ringan (superb)

# Konten
    title
    some article
    generate higher value
    welcome message
    great team
    portofolio
    table slide
    pricing plan
    result slide
    swot analysis
    detail project one week
    detail project one year
    chart slide
    proceed and timeline
    device mockup
    run process
    infographic slides

# Pengembangan konten
    calender
    maps (peta benua dan beberapa negara)
    
# Pengembangan slide
    style untuk tabel (masih manual, tapi bisa otomatis)
    transisi
    efek animasi info grafis

# Tidak memungkinkan :")
    blur
    transparansi dari svg
